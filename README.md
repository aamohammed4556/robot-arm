## Robot Arm and Inverse Kinematics

Code for a three jointed robot arm that is able to pick up things. Made with arduino

In the ``InverseKinematics`` folder, we made a simulation for determining arm angles using inverse kinematics

The webgui is still in development.

``SerialDataSending`` is just a testing folder for sending data from python scripts to the arduino serial port.

Right now, `main/main.ino` is the only one that works perfectly. 

Credit to https://github.com/Leek727 for helping build and code the robot
