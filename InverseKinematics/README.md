# InverseKinematics
## https://github.com/Leek727/InverseKinematics
Inverse kinematics but bad

Makes a table of calculated positions / angles for 3 segment robot arm.

Make sure to run `table_generator_no_visual.py`. Will take a while, but then `inverse_kinematics.py` should run as expected.

Also, `inverse_kinematics_serial.py` is unusable without an arduino.
