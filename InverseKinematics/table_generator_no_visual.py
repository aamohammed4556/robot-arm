import math
import time
import json
import numpy as np

# arm length
scale = 10
arm_lengths = [4.75 * scale, 4.5 * scale, 1.5 * scale]

# sums two vectors
def sum_vector(A, B):
    # A = (x, y)
    return (A[0] + B[0], A[1] + B[1])

# converts polar to the other one
def convert_cartesian(A):
    # A = (mag, angle)
    angle = (A[1] * math.pi) / 180
    mag = A[0]
    return (mag * math.cos(angle), mag * math.sin(angle))

data = {} # big table that stores positions to angles
times = []
start = time.time()
if __name__ == "__main__":
    # loop through every angle by step
    for a in range(0, 180, 1):
        s = time.time()
        for b in range(0, 180, 1):
            for c in range(0, 180, 1):
                # ------------------ forward kinematics part ------------------
                vector_array = [(arm_lengths[0], a), (arm_lengths[1], b+a), (arm_lengths[2], c+a+b)] # polar vectors

                # dont draw full arm
                cum_vector = [0,0]
                for vector in vector_array:
                    v1 = convert_cartesian(vector)
                    cum_vector = sum_vector(cum_vector, v1)


                # ------------------ hash map making part ------------------
                end_point = []
                key = str(round(cum_vector[0])) + "," + str(round(cum_vector[1]))
                try:
                    end_point = data[key]
                
                except:
                    data[key] = []

                end_point.append([a,b,c])

        print(f"{round((a / 180) * 100)}%")
        e = time.time() - s
        times.append(e)

        eta = np.average(times)
        print(f"ETA : {round(eta * ((180-a)/60), 4)} minutes left!")

print(f"Actual time = {(time.time() - start)/60} mins")   

print("Writing stuff...")
with open("vector_table.json", "w") as write_file:
    json.dump(data, write_file)

print("Done!")
