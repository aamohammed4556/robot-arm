#include <stdlib.h>
#include <Servo.h>
#define LEN_OF_DATA 100

char fullString[LEN_OF_DATA];
int index = 0;

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
Servo claw;

const int servo_len = 4;
const int runTimes = 2;

int looped = 0;

Servo servos[servo_len] = {servo1, servo2, servo3, servo1};
int pins[servo_len + 1] = {3, 5, 10, 11};
int offset[servo_len] = {0, -10, 0, 10};

int serial_pos[servo_len] = {90, 90, 90,  60};

String var1;
String var2;
String var3;
String var4;

void setup()
{

    for (int i = 0; i < servo_len; ++i)
    {
        servos[i].attach(pins[i]);
    }
    //claw.attach(9);
    //close_claw();
    Serial.begin(115200);
}

void loop()
{ 
    go_to_pos(serial_pos);
    delay(200);

    while (Serial.available() > 0)
    {
        var1 = Serial.readStringUntil(','); // writes in the string all the inputs till a comma
        Serial.read();
        var2 = Serial.readStringUntil(',');
        Serial.read();
        var3 = Serial.readStringUntil(','); // writes in the string all the inputs till the end of line character
        Serial.read();
        var4 = Serial.readStringUntil('\n'); // writes in the string all the inputs till the end of line character
        
        serial_pos[0] = var4.toInt();
        serial_pos[3] = var3.toInt();
        serial_pos[2] = var2.toInt();
        serial_pos[1] = var1.toInt();
        go_to_pos(serial_pos);
        delay(1000);
    }
}

void close_claw()
{
    for (int j = 0; j < 140; ++j)
    {
        claw.write(j);
        delay(5);
    }
}

void open_claw()
{

    for (int j = 0; j < 180; ++j)
    {
        claw.write(180 - j);
        delay(5);
    }
}

void go_to_pos(int end[])
{
    bool changed = false;
    while (true)
    {
        changed = false;
        // goes through each servo
        for (int i = 0; i < servo_len - 1; ++i)
        {
            // reads servo. If it is < the desired pos, it adds to the angle
            if (end[i] > servos[i].read())
            {
                servos[i].write(servos[i].read() + 1);
                changed = true;
            }
            // same as above but opposite.
            else if (end[i] < servos[i].read())
            {
                servos[i].write(servos[i].read() - 1);
                changed = true;
            }
        }
        delay(10);
        if (!changed)
        {
            servos[3].write(end[3]);
            break;
        }
    }
}
