import time
import serial

# Pretty self explanitory 
def foo(msg):
    # SET THE USB TO THE CORRECT ONE CUZ IT CHANGES
    ardu= serial.Serial('/dev/ttyUSB0',115200, timeout=.1)
    time.sleep(1)
    msg = ardu.write(f'{msg}'.encode())
    print(f"sent {msg}")
    time.sleep(1)
    #ardu.close()

while True:
    foo(input("send a thing >> "))
