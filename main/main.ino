#include <Servo.h>

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
Servo claw;

const int servo_len = 4;
const int runTimes = 2;

int looped = 0;

//Servo servos[servo_len] = {servo1, servo2, servo3, servo4};
//int pins[servo_len + 1] = {3, 5, 10, 11};
//int offset[servo_len] = {10, -10, 0, 0};
//int pos1[servo_len] = {60, 90, 90, 90};
//int pos2[servo_len] = {100, 115, 100, 60};
//int pos3[servo_len] = {25, 160, 100, 130};
Servo servos[servo_len] = {servo4, servo2, servo3, servo1};
int pins[servo_len + 1] = {11, 5, 10, 3};
int offset[servo_len] = {0, -10, 0, 10};
int pos1[servo_len] = {90, 90, 90, 60};
int pos2[servo_len] = {60, 115, 100, 100};
int pos3[servo_len] = {130, 160, 100, 25};

void setup()
{

  for (int i = 0; i < servo_len; ++i)
  {
    servos[i].attach(pins[i]);
  }
  claw.attach(9);
  //go_to_pos(pos1);
}

void loop()
{
  //move_like_crazy();
  if (looped < runTimes)
  {
    delay(1000);
    go_to_pos(pos1);
    
    delay(500);
    go_to_pos(pos2);
    delay(1500);
    close_claw();
    delay(1000);
    go_to_pos(pos1);
    delay(1000);
    go_to_pos(pos3);
    delay(1000);
    open_claw();
    ++looped;
  }
  else
  {
    go_to_pos(pos1);
  }
}

void close_claw()
{
  for (int j = 0; j < 140; ++j)
  {
    claw.write(j);
    delay(5);
  }
}

void open_claw()
{

  for (int j = 0; j < 180; ++j)
  {
    claw.write(180 - j);
    delay(5);
  }
}

void go_to_pos(int end[])
{
  bool changed = false;
  while (true) {
    changed = false;
    // goes through each servo
    for (int i=0; i<servo_len - 1; ++i) {
      // reads servo. If it is < the desired pos, it adds to the angle
      if (end[i] > servos[i].read()){
        servos[i].write(servos[i].read() + 1);
        changed = true;
      } 
      // same as above but opposite.
      else if (end[i] < servos[i].read()) {
        servos[i].write(servos[i].read() - 1);
        changed = true;
      }
    }
    delay(10);
    if (!changed) {
      servos[3].write(end[3]);
      break;
    }
  }
}

void move_pos()
{
  for (int i = 0; i < servo_len; ++i)
  {
    servos[i].write(pos1[i] + offset[i]);
    delay(20);
  }
  delay(2000);
  for (int i = 0; i < servo_len; ++i)
  {
    servos[i].write(pos2[i] + offset[i]);
    delay(20);
  }
  delay(2000);
}

void move_like_crazy()
{
  for (int j = 20; j < 145; ++j)
  {
    for (int i = 0; i < servo_len; ++i)
    {
      servos[i].write(j + offset[i]);
    }
    delay(50);
  }
  delay(500);

  for (int j = 20; j < 145; ++j)
  {
    for (int i = 0; i < servo_len; ++i)
    {
      servos[i].write(160 - j + offset[i]);
    }
    delay(50);
  }
  delay(500);
}
